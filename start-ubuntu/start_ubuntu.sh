#!/bin/bash
img_directory='/home/programmer/img' # путь к папке с фотографиями
interval=60 # интервал между снимками
size='640x480' # размеры фото

chmod -R 777 $img_directory
while true
do
    name=$(date +"%Y_%m_%d_%H%M")
    fswebcam -r $size $img_directory/$name.jpg
    sleep $interval
done
