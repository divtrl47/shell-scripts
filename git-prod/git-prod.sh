#!/bin/bash
PROJECT_NAME=$1
if [ -z $PROJECT_NAME ];
then
	echo "ERROR: Введите название проекта"
	exit
fi
echo $PROJECT_NAME@app-web.ru
ssh -o IdentitiesOnly=yes $PROJECT_NAME@app-web.ru &
echo "Подключились к $PROJECT_NAME"
git init
echo "Инициирован репозиторий"
git remote add origin $PROJECT_NAME@app-web.ru:~/repo/$PROJECT_NAME.app-web.ru.git
echo "Добавлена константа origin"
git pull origin master
echo "Получение данных"
if [ -z `ls -l .gitignore` ];
then
	echo "ERROR: Отсутствует файл .gitignore в репозитории."
	exit
fi
git add .
echo "добавляем все под гит"
git commit -am “init commit”
echo "Коммит готов"
git push origin master
echo "Данные отправлены в репозиторий"
